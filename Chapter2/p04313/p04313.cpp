/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;

const int INF = 0x7fffffff;

template <typename T>
class DoublyLinkedList {
private:
    struct Node {
        T data;
        Node* prev;
        Node* next;
        int freq;
        Node(const T& data) : data(data), prev(nullptr), next(nullptr), freq(0) {}
    };

    Node* head;

public:
    DoublyLinkedList() {
        head = new Node(T()); // 头结点不存储实际数据
        head->next = head;
        head->prev = head;
        head->freq = INF;
    }

    ~DoublyLinkedList() {
        Node* current = head->next;
        while (current != head) {
            Node* nextNode = current->next;
            delete current;
            current = nextNode;
        }
        delete head;
    }

    void append(const T& data) {
        Node* newNode = new Node(data);
        Node* tail = head->prev;
        tail->next = newNode;
        newNode->prev = tail;
        newNode->next = head;
        head->prev = newNode;
    }

    void display() const {
        Node* current = head->next;
        while (current != head) {
            std::cout << current->data << " ";
            current = current->next;
        }
        std::cout << std::endl;
    }

    Node* locateVal(T val) {
        Node* firstFreq = head;
        Node* curr = head;
        Node* location = head;
        while (true) {
            if (curr->next->data == val) {
                location = curr->next;
                curr->next = location->next;
                location->freq++;
                while (firstFreq->freq <= location->freq) {
                    firstFreq = firstFreq->prev;
                }
                location->next = firstFreq->next;
                firstFreq->next = location;
                return location;
            }
            else {
                curr = curr->next;
                if (curr->next) {
                    if (curr->next->freq != firstFreq->freq) {
                        firstFreq = curr->next;
                    }
                }
                else {
                    break;
                }
            }
        }
        return nullptr;
    }
};

class Solution {
public:

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
