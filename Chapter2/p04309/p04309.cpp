/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;

class ListNode {
public:
	int val;
	ListNode* next;
	ListNode() :val(0), next(nullptr) {}
	ListNode(int v, ListNode* p) :val(v), next(p) {}
};
typedef ListNode* LinkList;

class Solution {
public:
	// A,B两个集合，递增存于链表，求交集，保存到A中。
	void findSame(LinkList listA, LinkList listB) {
		assert(listA != nullptr);
		assert(listB != nullptr);
		ListNode* la = listA;
		ListNode* lb = listB;
		while (lb->next != nullptr && la->next != nullptr) {
			if (la->next->val == lb->next->val) {
				la = la->next;
				lb = lb->next;
			}
			else if (la->next->val < lb->next->val) {
				ListNode* temp = la->next;
				la->next = temp->next;
				delete  temp;
			}
			else { // (lb.next.val  < la.next.val)
				lb = lb->next;
			}
		}
		return;   
	}
};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
