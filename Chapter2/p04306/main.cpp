/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;

class ListNode {
public:
	int val;
	ListNode* next;
};


class Solution {
public:
	// ������ż�������
	void chaiFen(ListNode* head, ListNode* list1, ListNode* list2) {
		assert(list1->next == nullptr);
		assert(list2->next == nullptr);
		int cnt = 0;
		ListNode* temp;
		while (head->next) {
			if (cnt & 0x1) {
				list1->next = head->next;
				head->next = head->next->next;
				list1 = list1->next;
			}
			else {
				temp = head->next;
				head->next = temp->next;
				temp->next = list2->next;
				list2->next = temp;
			}
			cnt++;
		}
		list1->next = nullptr;
		list2->next = nullptr;
	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
