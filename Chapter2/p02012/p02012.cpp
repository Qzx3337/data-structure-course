/**********************
* p20-12
* 输入整数序列，输出主子元素
* 数据范围：0<= A[i] < n
* 主元素：出现次数超过 n/2 的元素
* 
********************/

#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>

using namespace std;

class Solution {
public:
	// 时间 O(n); 空间 O(n)。
	static int findMainElem(vector<int> &A) {
		int n = A.size();
		if (n == 0) return -1; // 空列表

		int* cnt = new int[n](); //  桶排，出现几次就标几
		int ans = A[0];
		for (int i = 0; i < n; i++) { // 遍历列表A
			cnt[A[i]]++;
			if (cnt[A[i]] > cnt[ans]) {
				ans = A[i];
			}
		}
		if (cnt[ans] <= n >> 1) ans = -1;  // 不存在 主元素
		delete[] cnt;
		return ans;
	}
};

int main()
{
	vector<int> data1{ 0,5,5,3,5,7,5,5 };
	vector<int> data2{ 0,5,5,3,5,1,5,7 };
	vector<int> data3{ 1,1,1,1,1,1,1,1 };
	vector<int> data4{ 0,1,2,3,4,5,6,7, };
	vector<int> data5{};

	cout << "Test 1: " << Solution::findMainElem(data1) << " (Expected: 5)" << endl;
	cout << "Test 2: " << Solution::findMainElem(data2) << " (Expected: -1)" << endl;
	cout << "Test 3: " << Solution::findMainElem(data3) << " (Expected: 1)" << endl;
	cout << "Test 4: " << Solution::findMainElem(data4) << " (Expected: -1)" << endl;
	cout << "Test 5: " << Solution::findMainElem(data5) << " (Expected: -1)" << endl;

	return 0;
}

