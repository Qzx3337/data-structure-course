#include <iostream>
#include <vector>

using namespace std;

// 摩尔投票算法
class Solution1 {
public:
    // 时间 O(n); 空间 O(1)。
    static int findMainElem(vector<int>& A) {
        int n = A.size();
        if (n == 0) return -1; // 空列表

        // 摩尔投票算法
        int candidate = -1;
        int count = 0;
        for (int num : A) {
            if (count == 0) {
                candidate = num;
                count = 1;
            } else if (num == candidate) {
                count++;
            } else {
                count--;
            }
        }

        // 验证候选者是否为主元素
        count = 0;
        for (int num : A) {
            if (num == candidate) {
                count++;
            }
        }
        return (count > n / 2) ? candidate : -1;
    }
};


#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;

// 随机化算法
class Solution2 {
public:
    static int findMajorityElement(vector<int>& nums) {
        srand(time(0));
        int n = nums.size();
        while (true) {
            int candidate = nums[rand() % n];
            if (count(nums.begin(), nums.end(), candidate) > n / 2) {
                return candidate;
            }
        }
        return -1; // 如果没有多数元素
    }
};



#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

// 扩展的摩尔投票算法
class Solution3 {
public:
    // 时间 O(n); 空间 O(k)。
    static vector<int> findMajorityElements(vector<int>& nums, int k) {
        unordered_map<int, int> candidates; // 用于存储候选者及其计数

        // 第一遍遍历：找到候选者
        for (int num : nums) {
            if (candidates.count(num)) {
                // 如果 num 已经是候选者，增加其计数
                candidates[num]++;
            }
            else if (candidates.size() < k - 1) {
                // 如果候选者数量小于 k-1，将 num 添加为候选者
                candidates[num] = 1;
            }
            else {
                // 如果候选者数量达到 k-1，减少所有候选者的计数
                for (auto it = candidates.begin(); it != candidates.end();) {
                    if (--(it->second) == 0) {
                        // 如果候选者的计数减为 0，将其移除
                        it = candidates.erase(it);
                    }
                    else {
                        ++it;
                    }
                }
            }
        }

        // 第二遍遍历：验证候选者
        unordered_map<int, int> counts; // 用于存储候选者的实际计数
        for (int num : nums) {
            if (candidates.count(num)) {
                // 统计候选者在数组中的实际出现次数
                counts[num]++;
            }
        }

        // 收集结果
        vector<int> result; // 用于存储最终的多数元素
        for (auto& candidate : candidates) {
            if (counts[candidate.first] > nums.size() / k) {
                // 如果候选者的实际出现次数超过 n/k，将其添加到结果中
                result.push_back(candidate.first);
            }
        }
        return result; // 返回多数元素
    }
};

