/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;


struct ListNode {
	int data;
	ListNode* next;
	ListNode(int x, ListNode* p) :data(x), next(p) {}
};


class Solution {
public:

	ListNode* pickNext(ListNode* p) {
		ListNode* temp = p->next;
		p->next = temp->next;
		return temp;
	}

	void insertNext(ListNode* head, ListNode* p) {
		p->next = head->next;
		head->next = p;
	}

	/// <summary>
	/// O(n),O(1)
	/// </summary>
	/// <param name="head">表头节点</param>
	void reSort(ListNode* head) {
		ListNode* head2 = new ListNode(0, nullptr);
		int len = 0;
		ListNode* it = head;
		// 找到表长
		for (it = head; it->next != nullptr; it = it->next) {
			len++;
		}
		// 找到表中点
		it = head;
		for (int i = 0; i < (len + 1) / 2; i++) {
			it = it->next;
		}
		// 头插过程逆序
		while (it->next != nullptr) {
			insertNext(head2, pickNext(it));
		}
		// 间隔插入回原表
		it = head->next;
		while (head2->next != nullptr) {
			assert(it != nullptr);
			insertNext(it, pickNext(head2));
			it = it->next->next;
		}

		delete head2;
	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
