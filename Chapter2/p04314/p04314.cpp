/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;

struct ListNode {
	int val;
	ListNode* next;
	ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
public:
	// 先连起来使链表循环，令头节点左移k，即右移n-k，然后在头节点前断开。
	// 时间O(n),空间O(1)
	void rotateRight(ListNode* head, int k) {
		// n>1; 0<k<n;
		ListNode* tail = head;
		int len = 1;
		while (tail->next) {
			tail = tail->next;
			len++;
		}
		tail->next = head;
		for (; len > k; len--) {
			tail = tail->next;
		}
		head = tail->next;
		tail->next = nullptr;
	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
