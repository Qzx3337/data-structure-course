/********
* 单链表，存m个整数
* -n <= data <= n
* 最佳时间
* data的模相等的仅保留第一个
* 
* 思路：桶排序
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;

struct ListNode {
	int data;
	ListNode* next;
	ListNode(int x, ListNode* p) :data(x), next(p) {}
};

class Solution {
public:
	/// <summary>
	/// 绝对值相同的节点保留第一个。
	/// 桶排序，O(m),O(n)。
	/// </summary>
	/// <param name="head">待处理链表头节点</param>
	/// <param name="n">data数据范围</param>
	void processList(ListNode* head, int n) {
		bool* flag = new bool[n+1]();
		
		for (ListNode* it = head; it->next != nullptr; ) {
			int v = abs(it->next->data);
			if (!flag[v]) {
				flag[v] = true;
				it = it->next;
			}
			else {
				ListNode* p = it->next;
				it->next = p->next;
				delete p;
			}
		}
		delete[] flag;
	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
