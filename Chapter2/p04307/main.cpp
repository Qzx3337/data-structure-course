/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;


class ListNode {
public:
	int val;
	ListNode* next;
	ListNode() :val(0), next(nullptr) {}
	ListNode(int v, ListNode* p) :val(v), next(p) {}
};


class Solution {
public:
	// 链表递增，删除重复元素
	bool removeDuplicate(ListNode* head) {
		if (head == nullptr) return false;  // 参数传入错误
		if (head->next == nullptr) return true; // 表为空
		ListNode* prev = head->next;
		ListNode* curr = prev->next;
		while (curr) {
			if (curr->val == prev->val) {
				prev->next = curr->next;
				delete curr;
				curr = prev->next;
			}
			else {
				prev = curr;
				curr = prev->next;
			}
		}
		return true;
	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
