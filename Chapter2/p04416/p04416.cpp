/********
*
********/

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <utility>
#include <vector>
#include <cassert>
#include <cstdio>
using namespace std;

struct ListNode {
	int val;
	ListNode* next;
	ListNode(int x, ListNode* p) : val(x), next(p) {}
};

class Solution {
public:
	int maxSumPair(ListNode* l) {
		int len = 1;
		for (; l->next != nullptr; l = l->next) {
			len++;
		}
		// l在尾节点处
		ListNode* head1 = new ListNode(0, l);
		ListNode* head2 = new ListNode(0, nullptr);
		ListNode* temp = nullptr;
		for (int i = 0; i < len / 2; i++) {
			temp = head1->next;
			head1->next = temp->next;
			temp->next = head2->next;
			head2->next = temp;
		}
		int maxSum = 0;
		ListNode* it1 = head1->next;
		ListNode* it2 = head2->next;
		if (len % 2 == 1) {
			maxSum = it1->val;
			it1 = it1->next;
		}
		while (it1 != nullptr) {
			maxSum = max(maxSum, it1->val + it2->val);
			it1 = it1->next;
			it2 = it2->next;
		}
		// 内存清理
		l->next = head2->next;
		delete head1;
		delete head2;
		return maxSum;
	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
