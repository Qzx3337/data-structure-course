/********
* 带头节点的链表
* 找倒数第K个节点
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;


struct ListNode {
	int val;
	ListNode* next;
	ListNode(int x, ListNode* p) : val(x), next(p) {}
};


class Solution {
public:
	// 不存在倒数第k个，返回null
	bool locateLastKNode(ListNode* head, int k) {
		ListNode* tail = head;
		for (int i = 0; i < k; i++) {
			if (tail == nullptr) {
				return false;
			}
			else {
				tail = tail->next;
			}
		}
		ListNode* ans = head;
		while (tail) {
			ans = ans->next;
			tail = tail->next;
		}
		if (ans == head) {
			return false;
		}
		else {
			assert(ans != nullptr);
			cout << ans->val << endl;
			return true;
		}
	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
