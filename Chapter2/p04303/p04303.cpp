/********
* 带头节点的单链表，写算法将其逆置。
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;

// 带头结点的单链表
class ListNode {
public:
	int val;
	ListNode* next;
	ListNode(int x) : val(x), next(NULL) {}
};

// 顺序遍历，在头节点插入
class Solution {
public:
	bool reverse(ListNode* head) {
		if (!head) return false;
		ListNode* firstNode = head->next;
		if (!firstNode) return true;
		while (firstNode->next) {
			ListNode* movingNode = firstNode->next;
			firstNode->next = movingNode->next;
			movingNode->next = head->next;
			head->next = movingNode;
		}
		return true;
	}
};

// 顺序遍历，修改指针。
class Solution2 {
public:
	bool reverse(ListNode* head) {
		// 检查头节点和头节点的下一个节点是否为空
		if (!head || !head->next) return false;

		ListNode* prev = nullptr; // 前一个节点，初始为 nullptr
		ListNode* curr = head->next; // 当前节点，从头节点的下一个节点开始

		// 遍历链表，逆置链表
		while (curr) {
			ListNode* next = curr->next; // 保存当前节点的下一个节点
			curr->next = prev; // 将当前节点的下一个节点指向前一个节点
			prev = curr; // 更新前一个节点为当前节点
			curr = next; // 移动到下一个节点
		}

		head->next = prev; // 将头节点的下一个节点指向新的头节点
		return true; // 返回 true 表示逆置成功
	}
};

/// <summary>
/// 两种解法都对，时空复杂度一样，但是sln2，三指针法，更利于理解，更推荐。
/// </summary>

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
