/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;


class ListNode {
public:
    int val;
    ListNode* prev;
    ListNode* next;
    ListNode(int x) : val(x), prev(nullptr), next(nullptr) {}
};

class CircularDoublyLinkedList {
private:
    ListNode* head;

public:
    CircularDoublyLinkedList() {
        head = new ListNode(0); // 头结点
        head->next = head;
        head->prev = head;
    }

    ~CircularDoublyLinkedList() {
        ListNode* current = head->next;
        while (current != head) {
            ListNode* temp = current;
            current = current->next;
            delete temp;
        }
        delete head;
    }

    // 插入到链表末尾
    void insert(int val) {
        ListNode* newNode = new ListNode(val);
        ListNode* tail = head->prev;

        tail->next = newNode;
        newNode->prev = tail;
        newNode->next = head;
        head->prev = newNode;
    }

    // 删除指定值的节点
    void remove(int val) {
        ListNode* current = head->next;
        while (current != head) {
            if (current->val == val) {
                current->prev->next = current->next;
                current->next->prev = current->prev;
                delete current;
                return;
            }
            current = current->next;
        }
    }

    // 遍历链表
    void traverse() {
        ListNode* current = head->next;
        while (current != head) {
            cout << current->val << " ";
            current = current->next;
        }
        cout << endl;
    }

    ListNode* getHead() {
        return head;
    }
};


class Solution {
public:
    bool checkSymmetry(CircularDoublyLinkedList& l) {
        auto ia = l.getHead()->next;
        auto ib = l.getHead()->prev;
        while (ia != ib && ia->next != ib) {
            if (ia->val != ib->val) {
                return false;
            }
            else {

                ia = ia->next;
                ib = ib->prev;

            }
        }
        return true;
    }
};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
