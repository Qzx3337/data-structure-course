/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;

class LinkNode {
public:
	int val;
	LinkNode* next;
};

class Solution {
public:
	bool removeBetween(LinkNode* head, int left, int right) {
		if (!head) return false;
		LinkNode* prev = head;
		LinkNode* curr = prev->next;
		while (prev->next) {
			curr = prev->next;
			if (left <= curr->val && curr->val < right) {
				prev->next = curr->next;
				delete curr;
			}
			else {
				prev = curr;
			}
		}
		return true;
	}
};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
