/********
* checked
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;


class ListNode {
public:
	int val;
	ListNode* next;
	ListNode() :val(0), next(nullptr) {}
	ListNode(int v, ListNode* p) :val(v), next(p) {}
};

class Solution {
public:
	// 给出有序链表A,B，根据A、B重复元素值，生成C
	bool makeFromDup(ListNode* listA, ListNode* listB, ListNode* listC) {
		if (!listA) return false;
		if (!listB) return false;
		if (!listC) listC = new ListNode();
		if (listC->next) return false;

		// 遍历A、B，尾插法C
		ListNode* pa = listA->next;
		ListNode* pb = listA->next;
		ListNode* pc = listC;
		while (pa && pb) {
			if (pa->val < pb->val) {
				pa = pa->next;
			}
			else if (pb->val < pa->val) {
				pb = pb->next;
			}
			else {// pa->val == pb->val
				if (pa->val == pc->val) {
					pa = pa->next;
					pb = pa->next;
				}
				else { // pa.val==pb.val && pa.val != pc.val
					pc->next = new ListNode(pa->val, nullptr);
					pc = pc->next;
					pa = pa->next;
					pb = pa->next;
				}
			}
		}
		return true;
	}
};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
