/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;


struct ListNode {
	int val;
	ListNode* next;
	ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
public:
	// 判断链表是否有环
	// 记录路径再恢复，O(n),O(n)
	bool checkCircle(ListNode* l) {
		assert(l != nullptr); // 判错输入
		if (l->next == nullptr) return false; // 判空

		bool ans = false; // 存答案
		vector<ListNode*> path; // 记录路径
		ListNode* prev = l;  // 循环遍历
		ListNode* curr = l->next; // 循环遍历
		ListNode* testNode = new ListNode(0); // 令所有节点都指向该测试用的根节点

		path.push_back(prev);
		path.push_back(curr);
		while (true) {
			// 退出条件
			if (curr->next == nullptr) {
				// 到达尾部， 恢复链表，无环
				ans = false;
				break;
			}
			else if (curr->next == testNode) {
				// 出现闭环，恢复 ，有环
				ans = true;
				break;
			}
			// 遍历
			prev->next = testNode;
			prev = curr;
			curr = curr->next;
			path.push_back(curr);
		}
		// 恢复链表
		for (int i = 0; i + 1 < path.size(); i++) {
			path[i]->next = path[i + 1];
		}
		delete testNode;
		return ans;
	}

};


class Solution2 {
public:
	// 标算判断环，快慢指针法。O(n),O(1)
	// 
	// 我们使用两个指针 slow 和 fast。slow 每次移动一步，而 fast 每次移动两步。
	// 如果链表中存在环，slow 和 fast 最终会相遇。
	// 
	// 此外，算法也可以找到环的起点：（证明写在书上）
	// 当快慢指针相遇时，将其中一个指针移到链表头部，
	// 然后两个指针每次都移动一步，直到它们再次相遇。
	// 相遇的节点即为环的起始节点。
	bool checkCircle(ListNode* l) {
		assert(l != nullptr); // Check for null input
		if (l->next == nullptr) return false; // Check for empty list

		ListNode* slow = l; // Slow pointer
		ListNode* fast = l; // Fast pointer

		while (fast != nullptr && fast->next != nullptr) {
			slow = slow->next; // Move slow pointer by one step
			fast = fast->next->next; // Move fast pointer by two steps

			if (slow == fast) {
				return true; // Cycle detected
			}
		}

		return false; // No cycle detected
	}
};

/// <summary>
/// 可能的需求是判断环、找到环的起点。
/// 快慢指针法都可以完成。
/// 记录路径目前没发现优势。
/// </summary>

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
