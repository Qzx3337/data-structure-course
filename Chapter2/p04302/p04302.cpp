/********
* 给出一个带头结点的单链表，设计一个算法删除链表中最小值的结点，假设该值唯一。
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;

// 类定义，带头结点的单链表
class ListNode {
public:
	int val;
	ListNode* next;
	ListNode() : val(0), next(NULL) {}
	ListNode(int x) : val(x), next(NULL) {}
	ListNode(int x, ListNode* next) : val(x), next(next) {}
	ListNode(vector<int> vals) :val(0), next(NULL) {
		ListNode* p = this;
		for (int i = 0; i < vals.size(); i++) {
			p->next = new ListNode(vals[i]);
			p = p->next;
		}
	}
};

class Solution {
public:
	bool deleteMinElem(ListNode* list, int val){
		assert(list != NULL);
		for (ListNode* it = list; it->next; it = it->next) {
			if (it->next->val == val) {
				ListNode* p = it->next;
				it->next = p->next;
				delete p;
				return  true;
			}
		}
		return false;
	}
};

class Test {
public:
	Test() {}
	~Test() {}

	bool makedata(vector<int>& data) {
		// 生成测试数据
		srand(static_cast<unsigned int>(time(0)));
		int size = rand() % 10 + 1; // 随机生成链表长度，1到10之间
		for (int i = 0; i < size; ++i) {
			data.push_back(rand() % 100); // 随机生成0到99之间的数
		}
		return true;
	}

	void run() {
		Solution s;
		vector<int> data;
		if (makedata(data)) {
			ListNode list(data);
			cout << "原始链表: ";
			printList(&list);

			// 找到最小值
			int minVal = findMin(&list);
			cout << "最小值: " << minVal << endl;

			// 删除最小值节点
			if (s.deleteMinElem(&list, minVal)) {
				cout << "删除最小值后的链表: ";
				printList(&list);
			}
			else {
				cout << "未找到最小值节点。" << endl;
			}
		}
	}

private:
	void printList(ListNode* list) {
		for (ListNode* it = list->next; it != NULL; it = it->next) {
			cout << it->val << " ";
		}
		cout << endl;
	}

	int findMin(ListNode* list) {
		int minVal = INT_MAX;
		for (ListNode* it = list->next; it != NULL; it = it->next) {
			if (it->val < minVal) {
				minVal = it->val;
			}
		}
		return minVal;
	}
};

int main(int argc, char const** argv) {
	Test t;
	t.run();

	return 0;
}
