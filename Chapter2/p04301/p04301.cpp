/**********************
* p43-01
* 输入：不带头节点的单链表类。
* 要求：删除所以值为x的节点，链表中含有多个x。
*
********************/

#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>

using namespace std;

// 不带头节点的单链表类
class ListNode {
public:
	int val;
	ListNode* next;
	ListNode() : val(0), next(NULL) {}
	ListNode(int x) : val(x), next(NULL) {}
	ListNode(vector<int> nums) : val(0), next(NULL) {
		if (nums.size() == 0) return;
		val = nums[0];
		ListNode* p = this;
		for (int i = 1; i < nums.size(); i++) {
			p->next = new ListNode(nums[i]);
			p = p->next;
		}
	}
};

class Solution {
public:
	ListNode* removeElements(ListNode* head, int val) {
		//if (head == NULL) {
		//	return NULL;
		//}
		ListNode** it = &head;
		for (;;) {
			if (*it == NULL) {
				break;
			}
			if ((*it)->val == val) {
				ListNode* p = *it;
				*it = p->next;
				delete p;
			}
			else {
				it = &(*it)->next;
			}
		}
		return head;
	}

};

// 虚拟头节点
class Solution2 {
public:
	ListNode* removeElements(ListNode* head, int val) {
		if (head == NULL) {
			return NULL;
		}
		ListNode* dummy = new ListNode(-1);
		dummy->next = head;
		ListNode* current = dummy;
		while (current->next != NULL) {
			if (current->next->val == val) {
				ListNode* temp = current->next;
				current->next = current->next->next;
				delete temp;
			}
			else {
				current = current->next;
			}
		}
		ListNode* newHead = dummy->next;
		delete dummy;
		return newHead;
	}
};




class testSolution {
public:
	static void printList(ListNode* head) {
		ListNode* current = head;
		while (current != NULL) {
			cout << current->val << " ";
			current = current->next;
		}
		cout << endl;
	}

	static void testRemoveElements() {
		vector<int> testCase1 = { 1, 2, 6, 3, 4, 5, 6 };
		vector<int> testCase2 = { 7, 7, 7, 7 };
		vector<int> testCase3 = { 1, 2, 3, 4, 5 };

		ListNode* head1 = new ListNode(testCase1);
		ListNode* head2 = new ListNode(testCase2);
		ListNode* head3 = new ListNode(testCase3);

		Solution sol1;
		Solution2 sol2;

		cout << "Original list 1: ";
		printList(head1);
		ListNode* result1_sol1 = sol1.removeElements(head1, 6);
		cout << "After removing 6 with sol1: ";
		printList(result1_sol1);

		head1 = new ListNode(testCase1); // 重新初始化链表
		ListNode* result1_sol2 = sol2.removeElements(head1, 6);
		cout << "After removing 6 with sol2: ";
		printList(result1_sol2);

		cout << "Original list 2: ";
		printList(head2);
		ListNode* result2_sol1 = sol1.removeElements(head2, 7);
		cout << "After removing 7 with sol1: ";
		printList(result2_sol1);

		head2 = new ListNode(testCase2); // 重新初始化链表
		ListNode* result2_sol2 = sol2.removeElements(head2, 7);
		cout << "After removing 7 with sol2: ";
		printList(result2_sol2);

		cout << "Original list 3: ";
		printList(head3);
		ListNode* result3_sol1 = sol1.removeElements(head3, 1);
		//ListNode* result3_sol1 = sol1.removeElements(NULL, 1);
		cout << "After removing 1 with sol1: ";
		printList(result3_sol1);

		head3 = new ListNode(testCase3); // 重新初始化链表
		ListNode* result3_sol2 = sol2.removeElements(head3, 1);
		cout << "After removing 1 with sol2: ";
		printList(result3_sol2);

		// 释放内存
		while (result1_sol1 != NULL) {
			ListNode* temp = result1_sol1;
			result1_sol1 = result1_sol1->next;
			delete temp;
		}
		while (result1_sol2 != NULL) {
			ListNode* temp = result1_sol2;
			result1_sol2 = result1_sol2->next;
			delete temp;
		}
		while (result2_sol1 != NULL) {
			ListNode* temp = result2_sol1;
			result2_sol1 = result2_sol1->next;
			delete temp;
		}
		while (result2_sol2 != NULL) {
			ListNode* temp = result2_sol2;
			result2_sol2 = result2_sol2->next;
			delete temp;
		}
		while (result3_sol1 != NULL) {
			ListNode* temp = result3_sol1;
			result3_sol1 = result3_sol1->next;
			delete temp;
		}
		while (result3_sol2 != NULL) {
			ListNode* temp = result3_sol2;
			result3_sol2 = result3_sol2->next;
			delete temp;
		}
	}
};



int main() {
	testSolution::testRemoveElements();

	return 0;
}
