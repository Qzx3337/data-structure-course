/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;


template <typename T>
class CircularLinkedList {
private:
    struct Node {
        T data;
        Node* next;
        Node(const T& data) : data(data), next(nullptr) {}
    };

    Node* head;
    Node* tail;

public:
    CircularLinkedList() : head(nullptr), tail(nullptr) {}

    ~CircularLinkedList() {
        if (head) {
            Node* current = head;
            do {
                Node* nextNode = current->next;
                delete current;
                current = nextNode;
            } while (current != head);
        }
    }

    void append(const T& data) {
        Node* newNode = new Node(data);
        if (!head) {
            head = newNode;
            tail = newNode;
            newNode->next = head;
        }
        else {
            tail->next = newNode;
            tail = newNode;
            tail->next = head;
        }
    }

    void display() const {
        if (!head) return;
        Node* current = head;
        do {
            std::cout << current->data << " ";
            current = current->next;
        } while (current != head);
        std::cout << std::endl;
    }

    void linkBack(CircularLinkedList& l) {
        tail->next = l.head;
        l.tail->next = head;
        tail = l.tail;
        l.head = nullptr;
        l.tail = nullptr;
        delete &l;
    }
};

class Solution {
public:
    void linkBtoA(CircularLinkedList <int>& a, CircularLinkedList <int>& b) {
        a.linkBack(b);
    }
};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
