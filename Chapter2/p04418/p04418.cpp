/********
* 链表存储单词
* 带头结点
* 共后缀
* best Time
* 找公共后缀起始位置
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;


struct ListNode {
	char val;
	ListNode* next;
	ListNode(char x, ListNode* p) : val(x), next(p) {}
};

class Solution {
public:
	/// <summary>
	/// 链表存储单词，带头结点，共后缀，best Time，找公共后缀起始位置
	/// </summary>
	/// <param name="head1">第一个单词头节点</param>
	/// <param name="head2">第二个单词头节点</param>
	/// <returns>公共节点位置</returns>
	ListNode* locateCommon(ListNode* head1, ListNode* head2) {
		/// 思路一：记录路径
		/// 遍历过的节点都指向一个根节点
		/// 慢指针扫描到根节点说明此处为公共开始处
		/// 利用路径恢复现场
		/// O(n),O(n)
		/// 
		/// 思路二：位移法
		/// 先遍历一遍找到二者长度
		/// 长的先走几步使得同步
		/// 二者同步走到相遇
		/// 
		/// 
		/// 标算为同步位移法
		/// 前面实现过，不实现了。
		;
	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
