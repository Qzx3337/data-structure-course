/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
#include <queue>
#include <stack>
using namespace std;


class Solution {
public:
	void myReverse(queue<int>& q) {
		stack<int>s;
		while (!q.empty()) {
			s.push(q.front());
			q.pop();
		}
		while (!s.empty()) {
			q.push(s.top());
			s.pop();
		}
	}
};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
