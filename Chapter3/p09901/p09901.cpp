/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
#include <stack>
using namespace std;


class Solution {
public:
	bool checkFomula(char* str) {
		stack<char>s;
		for (char* pc = str; *pc != '\0'; pc++) {
			switch (*pc) {
			case '(':
			case '[':
			case '{':
				s.push(*pc);
				break;
			case ')':
			case ']':
			case '}':
				if (s.empty()) {
					return false;
				}
				if (*pc == s.top()) {
					s.pop();
				}
				else {
					return false;
				}
				break;
			default:
				return false;
				break;
			}
		}
		return s.empty();
	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
