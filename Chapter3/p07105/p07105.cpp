/********
* 实现一个共享栈
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
using namespace std;

const int MAX_SIZE = 666;

class MyStruct {
public:
	int* first;
	int* second;
	int* firstTop;
	int* secondTop;
	int MAX_SIZE;

	MyStruct(int size) :MAX_SIZE(size) {
		first = new int[MAX_SIZE];
		firstTop = first;
		second = first + MAX_SIZE - 1;
		secondTop = second;
	}

	MyStruct() :MAX_SIZE(666) {
		first = new int[MAX_SIZE];
		firstTop = first;
		second = first + MAX_SIZE - 1;
		secondTop = second;
	}

	bool pushFirst(int x) {
		if (firstTop == secondTop) {
			cout << "\nmax size\n";
			return false;
		}
		*firstTop = x;
		firstTop++;
		return true;
	}

	bool popFirst() {
		if (first == firstTop) {
			cout << "\nempty stack\n";
			return false;
		}
		firstTop--;
	}
	bool pushSecond(int x) {
		if (firstTop == secondTop) {
			cout << "\nmax size\n";
			return false;
		}
		*secondTop = x;
		secondTop--;
		return true;
	}

	bool popSecond() {
		if (second == secondTop) {
			cout << "\nempty stack\n";
			return false;
		}
		firstTop++;
	}

};


class Solution {
public:

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
