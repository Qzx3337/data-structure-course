/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
#include <stack>
using namespace std;


struct ListNode {
	char data;
	ListNode* next;
	ListNode(char x, ListNode* p) :data(x), next(p) {}
};

class Solution {
public:

	bool checkSymmetry(ListNode* head) {
		vector<char> arr;
		for (ListNode* it = head->next; it != nullptr; it = it->next) {
			arr.push_back(it->data);
		}
		for (int i = 0; i < arr.size(); i++) {
			if (arr[i] != arr[arr.size() - 1 - i]) {
				return false;
			}
		}
		return true;
	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
