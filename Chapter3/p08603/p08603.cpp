/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
#include <stack>
using namespace std;


class OddQueue {
	//front - 1, 2, 3, 4 - rear
public:
	OddQueue() :state(atRear) {}

	void push(int x) {
		if (state == atFront) {
			while (!right.empty()) {
				left.push(right.top());
				right.pop();
			}
			state = atRear;
		}
		// at rear
		left.push(x);
	}

	bool pop(int& x) {
		if (state == atRear) {
			while (!left.empty()) {
				right.push(left.top());
				left.pop();
			}
			state = atFront;
		}
		//at front
		if (right.empty()) {
			return false;
		}
		else {
			x = right.top();
			right.pop();
			return true;
		}
	}

	bool empty() {
		if (left.empty() && right.empty()) {
			return true;
		}
		else {
			return false;
		}
	}

private:
	stack<int>left;
	stack<int>right;
	enum State {
		atRear,
		atFront
	}state;

};

class Solution {
public:

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
	}

};

int main(int argc, char const** argv) {
	Test t;

	return 0;
}
