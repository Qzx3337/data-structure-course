/*******************
* 
* language: C.
* 
**********************/
#define _CRT_SECURE_NO_WARNINGS 1
#include "p1.h"

#define MAXN 128

void Getnext(int next[], char* t)
{
    int i = 0;
    int j = -1;
    next[0] = -1;
    while (t[i] != '\0') {
        if (j == -1 || t[i] == t[j]) {
            ++i;
            ++j;
            next[i] = j;
        }
        else {
            j = next[j];
        }
    }
}

void printNext(int* next, int n)
{
	for (int i = 0; i <= n; ++i) {
		printf("%d\n", next[i]);
	}
}

int main(int argc, char const* argv[])
{
	freopen("p1.in", "r", stdin);
	char str[MAXN];
	int next[MAXN];

	gets(str);
	int n = strlen(str);
	Getnext(next, str);
	printNext(next, n);

	return 0;
}
