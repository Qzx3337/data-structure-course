#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cassert>

template <typename T>
class RadixSort {
public:
    // 构造函数，传入基数
    RadixSort(int base) : base(base) {
        assert(base > 1 && "Base must be greater than 1");
    }

    // 排序函数
    void sort(std::vector<T>& arr) {
        if (arr.empty()) return;

        // 找到最大值
        T maxVal = *std::max_element(arr.begin(), arr.end());

        // 计算最大值的位数
        int maxDigits = 0;
        while (maxVal > 0) {
            maxVal /= base;
            maxDigits++;
        }

        // 进行基数排序
        for (int digit = 0; digit < maxDigits; digit++) {
            countingSort(arr, digit);
        }
    }

private:
    int base;

    // 计数排序，按指定的位数进行排序
    void countingSort(std::vector<T>& arr, int digit) {
        int n = arr.size();
        std::vector<T> output(n);
        std::vector<int> count(base, 0);

        // 计算每个桶中的元素个数
        for (int i = 0; i < n; i++) {
            int bucket = (arr[i] / static_cast<int>(std::pow(base, digit))) % base;
            count[bucket]++;
        }

        // 计算累积计数
        for (int i = 1; i < base; i++) {
            count[i] += count[i - 1];
        }

        // 构建输出数组
        for (int i = n - 1; i >= 0; i--) {
            int bucket = (arr[i] / static_cast<int>(std::pow(base, digit))) % base;
            output[count[bucket] - 1] = arr[i];
            count[bucket]--;
        }

        // 复制输出数组到原数组
        for (int i = 0; i < n; i++) {
            arr[i] = output[i];
        }
    }
};

int main() {
    std::vector<int> arr = {170, 45, 75, 90, 802, 24, 2, 66};
    int base;

    std::cout << "Enter the base for radix sort: ";
    std::cin >> base;

    // 检查基数是否有效
    if (base <= 1) {
        std::cerr << "Base must be greater than 1" << std::endl;
        return 1;
    }

    RadixSort<int> sorter(base);
    sorter.sort(arr);

    std::cout << "Sorted array: ";
    for (int num : arr) {
        std::cout << num << " ";
    }
    std::cout << std::endl;

    return 0;
}
