/********
*
********/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <utility>
#include <vector>
#include <cassert>
#include <ctime>
#include <stack>
using namespace std;


class Solution {
public:

	int findFather(int x, int y) {
		if (x > y) {
			swap(x, y);
		}
		stack<int>sx;
		stack<int>sy;
		while (x) {
			sx.push(x & 1);
			x >>= 1;
		}
		while (y) {
			sy.push(y & 1);
			y >>= 1;
		}
		int ans = 0;
		while (!sx.empty()) {
			if (sx.top() == sy.top()) {
				// + ������ << ����������
				ans = (ans << 1) + sx.top();
				sx.pop();
				sy.pop();
			}
			else {
				break;
			}
		}
		return ans;

	}

};

class Test {
public:

	Test() {}
	~Test() {}

	bool makedata() {
		return true;
	}

	void run() {
		Solution s;
		cout << s.findFather(12, 7) << endl;
	}

};

int main(int argc, char const** argv) {
	Test t;
	t.run();

	//int x = 0 << 1;
	//cout << x << endl;

	return 0;
}
